# Flask es una lib para servidores Web
from flask import Flask
from flask import request
from flask import render_template

app = Flask(__name__,template_folder='templates')

@app.route('/',methods=['GET'])
def root():
    return render_template('index.html')

@app.route('/',methods=['POST'])
def root_post():
    name = request.form['name']
    return render_template(
        'index_post.html',
        name=name
    )

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
