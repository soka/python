# Flask es una lib para servidores Web
from flask import Flask
from flask import request

app = Flask(__name__)


@app.route('/',methods=['GET'])
def root():
    return """
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>HTML5 Basic Template</title>
    <meta name="description" content="HTML5 Basic Template">
    <meta name="author" content="Iker Landajuela">
</head>

<body>
    <h1>Hello Python!</h1>
    <form action="/"  method="POST">
        <input type="text" name="name" />
        <button>Submit</button>
    </form>
</body>
</html>
"""

@app.route('/',methods=['POST'])
def root_post():
    name = request.form['name']
    return f'''
        Estoy recibiendo un post name: {name}
        <a href="/">Volver</a>
        '''

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
