Flask init
==========

Guía rápida
-----------

Para arrancar la aplicación de este proyecto, ejecutar

```
./run python main.py
```

desde el directorio raíz del proyecto o, alternativamente, activar el menú `Terminal - Run Task` y seleccionar `main`

Estructura de este proyecto
---------------------------

El directorio raíz tiene los siguientes ficheros y directorios:

- `README.md`: este fichero.
- `requirements.txt`: fichero con los paquetes python necesarios para ejecutar la aplicación (1). Inicia con los paquetes `flask` y `pytest`.
- `Dockerfile`: definición de la imagen docker (2).
- `run`: comando para ejecutar la aplicación y otras utilidades.
- `main.py`: programa principal de la aplicación. Inicia con un servidor `flask` básico.
- `src`: directorio para el resto del código de la aplicación. Inicia vacía.
- `test`: directorio para los tests de la aplicación. Inicia vacía.
- `.vscode`: directorio con las tareas predefinidas `main` y `pytest` para VSCode (3).

El comando `./run`
----------------

El script `./run` construye y arranca una imagen docker con un sistema operativo básico con python incluido y está preparado para utilizarlo de la siguente forma:

- `./run` arranca la imagen en un terminal de linux
- `./run python` arranca la imagen en un intérprete de python
- `./run python main.py` arranca la imagen y ejecuta el archivo `main.py`

(es necesario iniciar el comando desde el directorio del proyecto)

Tareas personalizadas en VSCode
-------------------------------

El proyecto vienen con dos tareas personalizadas de VSCode.

- `main`: ejecuta el programa principal (`./run python main`)
- `test`: corre los test con la utilidad pytest (`./run python -m pytest`)

notas
-----
- (1) https://pip.pypa.io/en/stable/user_guide/#requirements-files
- (2) https://docs.docker.com/get-started/part2/
- (3) https://code.visualstudio.com/Docs/editor/tasks#_custom-tasks