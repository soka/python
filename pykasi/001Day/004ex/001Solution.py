#!/usr/bin/env python3

print('Yo he visto cosas que vosotros no creeríais.\n\tNaves de ataque en llamas más allá del hombro de Orión.\n\t\tHe visto rayos-C brillar en la oscuridad cerca de la Puerta de Tannhäuser.\nTodos esos momentos se perderán en el tiempo, como lágrimas en la lluvia.\n\tHora de morir.')

