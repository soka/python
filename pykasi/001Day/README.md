<!-- TOC -->

- [Ejercicio 1](#ejercicio-1)
    - [Solución 1](#solución-1)
- [Ejercicio 2](#ejercicio-2)
    - [Solución 1](#solución-1-1)
    - [Solución 2](#solución-2)
- [Ejercicio 3](#ejercicio-3)
    - [Solución 1](#solución-1-2)
- [Ejercicio 4](#ejercicio-4)
    - [Solución 1](#solución-1-3)
- [Ejercicio 5](#ejercicio-5)
    - [Solución 1](#solución-1-4)
- [Ejercicio 6](#ejercicio-6)
    - [Solución 1](#solución-1-5)
- [Ejercicio 7](#ejercicio-7)
    - [Solución 1](#solución-1-6)
- [Recursos](#recursos)

<!-- /TOC -->

Para resolver los ejercicios uso [https://repl.it/](https://repl.it/) con el mismo archivo en Python para no perder tiempo.

# Ejercicio 1

Buscar todos los números divisibles por 7 pero no por 5 entre 2000 y 3000 ambos incluidos, la salida debe ser una sola línea con todos los números que cumplan el criterio separados por ",".

## Solución 1

Usar un bucle [`For`](https://www.w3schools.com/python/python_for_loops.asp) con la función [`range(start, stop)`](https://www.w3schools.com/python/ref_func_range.asp).

Dentro del bucle para conocer si un número es divisible por otro usamos el operador aritmético modulo [`%`](https://www.w3schools.com/python/python_operators.asp) y el operador condicional `if` para comparar si el resto es 0 con comparadores lógicos `==` o `!=`. 

[Ver solución](./001ex/001Solution.py)

---
---
---

# Ejercicio 2

Imprime por consola los números pares comprendidos en el intervalo 0 a 10 ambos incluidos.

## Solución 1

Usar el bucle [`For`](https://www.w3schools.com/python/python_for_loops.asp) y la función [`range(start, stop, step)`](https://www.w3schools.com/python/ref_func_range.asp) con un 3º parámetro opcional `step` para especificar el incremento en cada iteración.

[Ver solución](./002ex/001Solution.py)

## Solución 2

Usar un bucle [`For`](https://www.w3schools.com/python/python_for_loops.asp) con la función [`range(start, stop)`](https://www.w3schools.com/python/ref_func_range.asp) y usar el operador aritmético [`%`](https://www.w3schools.com/python/python_operators.asp) con el operador lógico `==` para saber si el resto es 0.

Probablemente esta solución es menos eficiente que la anterior en tiempo de computación ya que debe realizar más iteraciones y además comprobar cada número.

[Ver solución](./002ex/002Solution.py)

---
---
---

# Ejercicio 3

**Función** que reciba un número y retorne su factorial, el número debe ser introducido por el usuario.

## Solución 1

- Usa la recursividad en la función.
- Usa la función [`input`](https://www.w3schools.com/python/ref_func_input.asp) para leer el valor como cadena y hacer un casting a [`int()`](https://www.w3schools.com/python/ref_func_int.asp).

[Ver solución](./003ex/001Solution.py)

---
---
---

# Ejercicio 4

Escribe la siguiente frase de Blade Runner pero la salida debe tener este formato y usando sólo una vez la función print.

```py
Yo he visto cosas que vosotros no creeríais. 
    Naves de ataque en llamas más allá del hombro de Orión. 
        He visto rayos-C brillar en la oscuridad cerca de la Puerta de Tannhäuser. 
Todos esos momentos se perderán en el tiempo, como lágrimas en la lluvia. 
    Hora de morir.
```

## Solución 1 

Usa los [carácteres de escape](https://www.w3schools.com/python/gloss_python_escape_characters.asp).

[Ver solución](./004ex/001Solution.py)

---
---
---

# Ejercicio 5

Dado un radio introducido por el usuario calcular el área de un círculo.

## Solución 1

- La constante pi se puede encontrar en la librería math
- Se puede elevar un número a una potencia con [`pow`](https://www.w3schools.com/python/ref_func_pow.asp) o el equivalente más corto con  `**` (por ejemplo `2**3` es `2*2*2`).

[Ver solución](./005ex/001Solution.py)

---
---
---

# Ejercicio 6

Programa que acepte una secuencia de números separados por coma y que genere una lista (no deben ser todos los elementos del mismo tipo) 

## Solución 1

- Usar la función [`split`](https://www.w3schools.com/python/ref_string_split.asp) especificando el separador "," como parámetro para crear la lista
- Para imprimirla se pueden usar varios [métodos](https://www.geeksforgeeks.org/print-lists-in-python-4-different-ways/), yo he usado la función [`join`](https://www.w3schools.com/python/ref_string_join.asp)

[Ver solución](./006ex/001Solution.py)

---
---
---

# Ejercicio 7

Programa que obtenga el primer y último elemento de esta lista `color_list = ["Red","Green","White" ,"Black"]`

## Solución 1

Usa la función `len` para obtener el número de elementos de la lista.

[Ver solución](./007ex/001Solution.py)

---
---
---

# Recursos

- [https://github.com/darkprinx/100-plus-Python-programming-exercises-extended/blob/master/Status/Day%201.md](https://github.com/darkprinx/100-plus-Python-programming-exercises-extended/blob/master/Status/Day%201.md)
- [https://www.w3resource.com/python-exercises/python-basic-exercises.php](https://www.w3resource.com/python-exercises/python-basic-exercises.php)