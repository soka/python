Ejercicios con [listas](https://www.w3schools.com/python/python_lists.asp).

Las listas son colecciones de elementos, pueden ser de distinto tipo como por ejemplo `["abc", 34, True, 40, "male"]`. 

<!-- TOC -->

- [Ejercicio 1](#ejercicio-1)
    - [Solución 1](#solución-1)
- [Ejercicio 2](#ejercicio-2)
    - [Solución 1](#solución-1-1)
- [Ejercicio 3](#ejercicio-3)
    - [Solución 1](#solución-1-2)
- [Ejercicio 4](#ejercicio-4)
    - [Solución 1](#solución-1-3)
- [Ejercicio 5](#ejercicio-5)
    - [Solución 1](#solución-1-4)
- [Ejercicio 6](#ejercicio-6)
    - [Solución 1](#solución-1-5)
- [Ejercicio 7](#ejercicio-7)
    - [Solución 1](#solución-1-6)
- [Recursos](#recursos)

<!-- /TOC -->

# Ejercicio 1

Imprime el segundo elemento de esta lista `fruits = ["apple", "banana", "cherry"]`

## Solución 1

Este ejercicio es muy sencillo, sólo hay que recordar que las posiciones de una lista comienzan contando desde 0, usamos `[0]` para acceder a un determinado elemento, `[2]` para el segundo, etc.

[Ver solución](./001ex/001Solution.py)

# Ejercicio 2

Obtén el número de elementos de la lista `fruits = ["apple", "banana", "cherry"]`. 

## Solución 1

Con la función [`len`](https://www.w3schools.com/python/ref_func_len.asp)

[Ver solución](./002ex/001Solution.py)

# Ejercicio 3

Imprime por consola (`print`) el tipo de una variable tipo lista `["apple", "banana", "cherry"]`.

## Solución 1

Usando la función [`type()`](https://www.w3schools.com/python/ref_func_type.asp).

[Ver solución](./003ex/001Solution.py)

Debe imprimir esto `<class 'list'>`.

# Ejercicio 4

Imprime el último elemento de esta lista `[1,2,3,4]` usando la indexación negativa.

## Solución 1

[Ver solución](./004ex/001Solution.py)

# Ejercicio 5

Usa el método [`append`](https://www.w3schools.com/python/ref_list_append.asp) para añadir el elemento 'xbox' a la siguiente lista `gift_list=['socks', '4K drone', 'wine', 'jam']` en la última posición.

## Solución 1

[Ver solución](./005ex/001Solution.py)

# Ejercicio 6

Las listas pueden contener elementos de diferentes tipos de datos, una lista puede tener incluso otra lista anidada dentro.
Añade la lista `["socks", "tshirt", "pajamas"]` dentro de `gift_list=['socks', '4K drone', 'wine', 'jam']`.

El resultado final al imprimir la nueva lista debería ser `['socks', '4K drone', 'wine', 'jam', ['socks', 'tshirt', 'pajamas']]`.

## Solución 1 

[Ver solución](./006ex/001Solution.py)

# Ejercicio 7

El método [`insert(index,element)`](https://www.w3schools.com/python/ref_list_insert.asp) permite añadir un nuevo elemento en una posición determinada.

Inserta "slippers" en la posición 3 de la lista `gift_list=['socks', '4K drone', 'wine', 'jam']`.

## Solución 1 

[Ver solución](./007ex/001Solution.py)

# Recursos

- [https://holypython.com/beginner-python-exercises/exercise-6-python-lists/](https://holypython.com/beginner-python-exercises/exercise-6-python-lists/)





