#!/usr/bin/env python3

################## 001DAY - 002 Exercise - 001 Solution ##################################

arr = [64, 34, 25, 12] 
n = len(arr) 
  
for i in range(n-1): 
    for j in range(0, n-i-1):   
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j] 
print ("Sorted array is:",arr) 