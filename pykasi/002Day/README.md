
Los siguientes ejercicios son los típicos que te enseñan en cualquier lenguaje de programación en la carrera, recuerdo que muchos de ellos los repetía hasta la saciedad para aprender C, usando la síntaxis básica de bucles te permite coger velocidad programando, pero sobre todo te obligan a desarrollar un pensamiento lógico y a entender lo que estas haciendo, al menos si no quieres tirarte un rato usando el método de prueba y error al azar. Te sugiero que cogas bolígrafo y papel y dibujes.

<!-- TOC -->

- [Ejercicio 1](#ejercicio-1)
    - [Solución 1](#solución-1)
- [Ejercicio 2](#ejercicio-2)
    - [Solución 1](#solución-1-1)
- [Recursos](#recursos)

<!-- /TOC -->

# Ejercicio 1

Este y otros problemas parecidos son un clásico en programas para consola, se trata de pintar  una forma geométrica en la pantalla usando carácteres ASCII. Dada una altura introducida por el usuario (líneas que debemos imprimir) se trata de pintar un triangulo invertido relleno de carácteres "*" de esa altura. Este problema sólo usa bucles for anidados, la 'dificultad' del mismo reviste en encontrar el patrón y la progresión para imprimir los espacios y los "*" precisos en cada línea.

Ejemplo para altura 4, fijate que la anchura de la base es dos veces la altura - 1.

```bash
*******
 ***** 
  ***  
   *   
```

## Solución 1

- Primero recorremos en un bucle for desde 0 hasta la altura - 1 (usando función range).
- En cada altura o línea se imprimirán tantos espacios como el número de línea en el que estamos (que comienza a contar desde 0).
- El número de carácteres "*" en cada línea será la `(altura * 2) -1`.
- Al final de la iteración del bucle for principal restamos una unidad a la altura.

[Ver solución](./001ex/001Solution.py)

---
---
---

# Ejercicio 2

Existen diferentes algoritmos para ordenar una lista de números en orden ascendente o descendente. Entre estos algoritmos uno de los más sencillos es el [método de la burbuja](https://es.wikipedia.org/wiki/Ordenamiento_de_burbuja) (_bubble short_), tal vez no sea el más eficiente en tiempo de ejecución pero sin duda es mi preferido, una vez que lo entiendes ya no olvidas el método.

El método de la burbuja funciona revisando cada elemento de la lista que va a ser ordenada con el siguiente, intercambiándolos de posición si están en el orden equivocado, por ejemplo si estamos ordenando de forma ascendente una secuencia de números de menor a mayor comparamos el número en primera posición con el siguiente, si es mayor los intercambiamos, ahora hacemos lo mismo entre la segunda y tercera posición y seguimos así hasta comparar todos, al final de la primera iteración el número más grande ya está posicionado al final de la  lista. 

Este algoritmo obtiene su nombre de la forma con la que suben por la lista los elementos durante los intercambios, como si fueran pequeñas "burbujas". También es conocido como el método del intercambio directo.

## Solución 1

Vamos a usar dos bucles for anidados, el primer for recorre todos los elementos de la lista, el segundo anidado dentro del anterior recorre el número de elementos de la lista restando el contador del primer bucle. Cuando ya ha ordenado o subido el elemento más grande al final de la lista ya no necesita compararlo, y asi cada iteración tiene que comparar menos elementos ya que los últimos estarán ordenados.

vamos a imaginar que tenemos la siguiente lista `[64, 34, 25, 12]` con 4 elementos. "i" es el contador del bucle principal y "j" el anidado. En la primera iteracción compara el primer elemento de la lista con todos los demás, como es el más grande se va intercambiando hasta acabar al final de la lista. 

La segunda iteración del principal ya no debemos comparar todos los elementos ya que hemos movido el más grande al final y ya no hace falta compararlo. Lo ilustro en la siguiente tabla para que se entienda mejor, no he completado todas las iteraciones pero creo que se entiende la lógica básica.

| i | j | lista entrada    | lista salida       |
|---|---|------------------|--------------------|
| 0 | 0 | [64, 34, 25, 12] | [34, 64, 25, 12]   |
| 0 | 1 | [34, 64, 25, 12] | [34, 25, 64, 12]   |
| 0 | 2 | [34, 25, 64, 12] | [34, 25, 12, 64]   |
| 0 | 3 | [34, 25, 12, 64] | [34, 25, 12, 64]   |
| 1 | 0 | [34, 25, 12, 64] | [25, 34, 12, 64]   |
| 1 | 1 | [25, 34, 12, 64] | [25, 12, 34, 64]   |

[Ver solución](./002ex/001Solution.py)

---
---
---

# Recursos
