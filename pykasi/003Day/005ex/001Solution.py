#!/usr/bin/env python3

###################### 003DAY - 005 Excercise - 001 Solution ########################################

import unittest 

def reverse(my_str):
    ret_str = "" 
    for i in my_str: 
        ret_str = i + ret_str
    return ret_str

print(reverse('Hello World'))

assert reverse('Hello World') == 'dlroW olleH'
