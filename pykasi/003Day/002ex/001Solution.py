#!/usr/bin/env python3


###################### 003Day - 002Exercise - 001Solution ########################################   

def get_cap(my_str):
    cont = 0
    for i in my_str:
        ascii_val = ord(i)
        if ((ascii_val>=65) and (ascii_val<=90)):
            cont += 1
    return cont

my_str = 'Write a Python Program'
print('Capital letters number: ',get_cap(my_str))

