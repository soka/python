Cadenas (_strings_) de carácteres.

<!-- TOC -->

- [Ejercicio 1](#ejercicio-1)
    - [Solución 1](#solución-1)
- [Ejercicio 2](#ejercicio-2)
    - [Solución 1](#solución-1-1)
- [Ejercicio 3](#ejercicio-3)
    - [Solución 1](#solución-1-2)
- [Ejercicio 4](#ejercicio-4)
    - [Solución 1](#solución-1-3)
- [Ejercicio 5](#ejercicio-5)
    - [Solución 1](#solución-1-4)
    - [Solución 2](#solución-2)
- [Recursos](#recursos)

<!-- /TOC -->

# Ejercicio 1

Dada una cadena de texto cualquiera por ejemplo 'Print more than one object' escribir una función "get_str_len" que reciba esta variable y retorne su longitud (sin usar la función [`len`](https://www.w3schools.com/python/ref_func_len.asp)).

## Solución 1

Podemos recorrer los elementos de la cadena la cadena con un bucle [`For`](https://www.w3schools.com/python/python_for_loops.asp) y usar una variable numérica para calcular la longitud total incrementando una unidad su valor en cada iteración.

[Ver solución](./001ex/001Solution.py)

---
---
---

# Ejercicio 2

Dada una cadena cualquiera 'Write a Python Program', escribir una función que reciba la cadena de texto como entrada y retorne **el número de carácteres mayúsculas que contiene**.

## Solución 1

Como curiosidad, cada carácter en codificación [ASCII](https://es.wikipedia.org/wiki/ASCII) se representa con un byte (8 bits) por lo tanto podemos tener un conjunto de 255 carácteres diferentes, entre ellos los dígitos de 0 al 9, carácteres de la 'a' a la 'z' en minusculas y de la 'A' a la 'Z' en mayúsculas, también otro tipo de carácteres no imprimibles de control como '\n' para comenzar una nueva línea (se le llama LF de line feed o NL New Line).

En la tabla [ASCII](https://es.wikipedia.org/wiki/ASCII) cada carácter tiene una posición determinada, el carácter 'A' tiene un valor decimal de 65 y 'Z' de 90, si transformamos un carácter de la cadena en su equivalente númerico podemos comparar en una sentencia lógica [`if`](https://www.w3schools.com/python/python_conditions.asp) si está dentro de ese intervalo.

Para obtener el **valor decimal ASCII de un carácter** podemos usar la función [`ord`](https://www.w3schools.com/python/ref_func_ord.asp)

![](img/ascii-codes-table.png)

[Ver solución](./002ex/001Solution.py)

---
---
---

# Ejercicio 3

Dada una cadena de texto 'Write a Python Program' escribir una función que reciba la cadena y convierta las minúsculas a mayúsculas y a la inversa.

## Solución 1

Volvemos a usar usar la función [`ord`](https://www.w3schools.com/python/ref_func_ord.asp) para obtener el valor numérico ASCII de un carácter.

Los carácter 'a' a la 'z' tienen valores ASCII 97 a 122, en mayúsculas entre 65 y 90, es decir hay una diferencia de 32, si sumamos por ejemplo 32 al valor ASCII de 'A' que es 65 el resultado es 97, exactamente la 'A' mayúscula.

[Ver solución](./003ex/001Solution.py)

---
---
---

# Ejercicio 4

Escribir una función que reciba una cadena y un carácter y borre todas las ocurrencias de ese carácter en la cadena.

## Solución 1

Vamos a usar el método [`replace()`](https://www.w3schools.com/python/ref_string_replace.asp) de la clase String.

[Ver solución](./004ex/001Solution.py)

---
---
---

# Ejercicio 5

Programa una función llamada "reverse" que pasando una cadena de texto como parámetro la devuelva con el orden invertido.

Ejemplo:

```py
reverse('Hello World') == 'World Hello'
```

## Solución 1

[Ver solución](./005ex/001Solution.py)

## Solución 2

Otra solución es usar la recursividad, la posición `x[-1]` es el último carácter, `x[:-1]` es toda la cadena excepto el último carácter. En [Stackoverflow](https://stackoverflow.com/questions/5532902/python-reversing-a-string-using-recursion) se proponen diferentes variaciones de este algoritmo.

[Ver solución](./005ex/002Solution.py)

# Recursos

- [https://www.geeksforgeeks.org/reverse-string-python-5-different-ways/](https://www.geeksforgeeks.org/reverse-string-python-5-different-ways/)
- [https://www.w3schools.com/python/python_howto_reverse_string.asp](https://www.w3schools.com/python/python_howto_reverse_string.asp)
- [https://www.educative.io/edpresso/how-do-you-reverse-a-string-in-python](https://www.educative.io/edpresso/how-do-you-reverse-a-string-in-python)
- [https://www.geeksforgeeks.org/find-length-of-a-string-in-python-4-ways/](https://www.geeksforgeeks.org/find-length-of-a-string-in-python-4-ways/)
- [https://www.w3resource.com/python-exercises/string/](https://www.w3resource.com/python-exercises/string/)
- [https://www.codewars.com/collections/easy-python-katas](https://www.codewars.com/collections/easy-python-katas)