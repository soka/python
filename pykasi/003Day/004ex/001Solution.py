#!/usr/bin/env python3

###################### 003Day - 004Excercise - 001Solution ########################################

def del_all_ocurrences(my_str,ch):
    return my_str.replace(ch,'')

str_text = "Delete all occurrences of a specified character in a given string"
print("String with character 'a' deleted: "+del_all_ocurrences(str_text,'a'))


