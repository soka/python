#!/usr/bin/env python3

###################### EJERCICIO 3 ########################################


def swap_cases(my_str):
    new_str=''    
    for i in my_str:
        ascii_val = ord(i)        
        if ((ascii_val>=65) and (ascii_val<=90)):
            ascii_val += 32
        else:
            ascii_val -= 32        
        new_str += chr(ascii_val)        
    return new_str

my_str = 'Write a Python Program'
print('Swap cases result: '+swap_cases(my_str))

