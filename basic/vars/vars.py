#!/usr/bin/env python3

# https://entrenamiento-python-basico.readthedocs.io/es/latest/leccion3/

# Inicialización de más de una variable en una línea
a,b,c = 12.3,'Hola',4
# Mismo valor a diferentes variables
x = y = z = True

# No hay declaración explicita del tipo de dato que contiene (al contrario de C y otros lenguajes)

# Ayuda integrada
#help('print')

##########################
# NUMÉRICOS
##########################

print('==== Enteros ======')
var_int = 21
var_int ="string" # Esto se puede hacer

var_int = 21
print(type(var_int))
# En un script produce fallo el "L"
#var_int_long = 21L
#print(type(var_int_long)) 
# Se puede definir como octal anteponiendo un 0 o hex con 0x

del(var_int)

# Produce error NameError: name 'var_int' is not defined
# print(var_int) 

print('======= Flotantes ==========')
float_1, float_2, float_3 = 0.348, 10.5, 1.5e2
print(float_1,type(float_1))
print(float_2, type(float_2))
print(float_3, type(float_3))

# Conversión de números
# Recibe un número o una cadena y lo transforma, si el parámetro es un float lo trunca
# El ejemplo de abajo imprime 7
print(int(7.89))

##########################
# CADENA
##########################

# Una cadena de texto, no hace declaración 
# explicita de tipo como en otros lenguajes
var_str="cadena"
#var_str="cambio cadena"
print(type(var_str)) # <class 'str'>

# Las comillas triples permiten definir cadenas de varias líneas
print('''Esto es una cadena
que ocupa
varias líneas''')

var_str='cadena'
print(var_str[0]) # Podemos acceder a sus elementos como una lista o tupla
print(var_str+" de TEST") # Concatenamos 

# Acceso a métodos
var_str.capitalize()

my_int = 10
print(f"My int is: {my_int}")

entero = 12
cadena = "Mi entero es: {}"
print(cadena.format(entero))

# Más ejercicios imprimiendo cadenas 
print("Su pelo era blanco como la {} de la cima del monte {}.".format('nieve','everest'))

c1,c2,c3,c4='H','o','l','a'
print(c1+c2+c3+c4)

formatter = "{} {} {} {}"
print(formatter.format(1, 2, 3, 4))
print(formatter.format("one", "two", "three", "four"))
print(formatter.format(True, False, False, True))
print(formatter.format(formatter, formatter, formatter,
formatter))
print(formatter.format(
"Try your",
"Own text here",
"Maybe a poem",
"Or a song about fear"
))

# Secuencias de escape
print("línea1\nlínea2\nlínea3")

cadena_tab = "\tEstoy tabulada."
print(cadena_tab)

# operadores aritméticos con cadenas
x = 'uno' + ' dos' + ' tres'
print(x)

print(x*2)

# Obteniendo valores

# La función input() siempre devuelve un valor numérico:
edad = input('Deme su edad: ')
print(edad)

## La función raw_input() siempre devuelve un valor de cadenas de caracteres:
cadena_usuario = input('Nombre de usuario:')
print(cadena_usuario)

##########################
# BOOLEANO
##########################

# Booleano True o False
var_bool = True 
print(type(var_bool))

print('El valor de True es: ',True)

# Operadores logicos
bAnd = True and False
print(bAnd)


##########################
# TUPLA ()
##########################

# Se pueden considerar como listas de sólo lectura.
# Los elementos y tamaño no se puede cambiar
# Los elementos pueden ser de diferente tipo
var_tuple = (12,23,345,10)

print(var_tuple) # Imprime todo el contenido
print(var_tuple[0]) # El primer elemento
print(var_tuple[1:3]) # Desde elemento dos hasta 3 -> 23,345
print(var_tuple[2:])  # Imprime 345,10

var_tuple_2 = ("uno",1, 2.33,'elemento')
print(var_tuple*2) # Imprime dos veces la lista 

##########################
# LISTAS []
##########################

# Los elementos pueden ser de distinto tipo y el nº de elementos 
# variable 
var_lista = [1,'str',23.3,'hola']
print(var_lista)

my_list = list()
my_list.append("Hello")
my_list.append(" world")
print(my_list)

##########################
# DICCIONARIO
##########################

# Funcionan como matrices asociativas con parámetros clave-valor
var_dict = {}
var_dict['uno'] = 'valor'
var_dict[2] = 'dos'

print(var_dict)

tiny_dict = {'uno':1,'dos':'dos',3:'tres'}
print(tiny_dict)
print(tiny_dict.keys())
print(tiny_dict.values())











