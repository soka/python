#!/usr/bin/env python3

# Previamente tal vez debamos instalar PIP para gestionar los paquetes de Python
# sudo apt install python3-pip

# Ahora instalamos el paquete necesario Pathlib
# pip3 install pathlib

from pathlib import *

# Muestra los contenidos del directorio actual

currentDir = Path('.')

for currentEl in currentDir.iterdir():
    print(currentEl)

currentFile = Path('./pathlib-intro.py')
print(currentFile.parent)

