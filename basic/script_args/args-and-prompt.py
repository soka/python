#!/usr/bin/env python3

from sys import argv

cadena_nombre_script, cadena_usuario = argv
print(f'Hola {cadena_usuario}, me llamo {cadena_nombre_script}')

# En python v2 raw_input, para v3 input
cont = input('¿Quieres continuar?[y|n]')

if cont == 'y':
    print('continuar')
else:
    print('no continuar')


