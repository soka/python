#!/usr/bin/env python3

def func(num):
    return num*num

res = func(2)
print(res)


def hola(name):
    return print("hola ",name)

hola('iker')

# Argumentos por defecto
def def_ars(a=2,b=2):
    return a*b

def_args(,)